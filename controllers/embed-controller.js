(function(window, angular, undefined){
  angular.module('embedApp')
    .controller('embedController', ['$scope', '$http', '$location', '$timeout',
      function(                      $scope,   $http,   $location,   $timeout){
        $scope.gotNotification = false;
        $scope.notifications = [];
        var username = $location.search().username;
        var socket = window.io.connect('http://localhost:8080/');
            socket.emit('connect-broadcaster', username);


        //This will get the user's settings from the node server and store it to memory
        /*
          $http.get()
         */
        $scope.followerData =  {
          notificationLayout:'Image to left of text',
          notificationTextColor:'#00FF00',
          alertDelay:3,
          followerAlertEnabled:false,
          followerAlertAnimation:'Slide Down',
          followerVolume:5,
          followerMessage:'Thank you for following!',
          followerMessageAnimation:'No Animation',
          followerFont: 'Exo 2',
          followerFontSize: '30px',
          followerFontWeight: 500,
          followerTextColor: '#FFFFFF',
          followerAlertDuration:5,
          followerImage: undefined
        };

        $scope.subscriberData = {
          notificationLayout:'Image to left of text',
          notificationTextColor:'#00FF00',
          alertDelay:3,
          subscriberAlertEnabled:false,
          subscriberAlertAnimation:'Slide Down',
          subscriberVolume:5,
          subscriberMessage:'Thank you for following!',
          subscriberMessageAnimation:'No Animation',
          subscriberFont: 'Exo 2',
          subscriberFontSize: '30px',
          subscriberFontWeight: 500,
          subscriberTextColor: '#FFFFFF',
          subscriberAlertDuration:5,
          subscriberImage: undefined
        };

        //This will look for people following the application
        socket.on('overlay_' + username, function(user){
          console.log("this is the new action ", user);
          $scope.notifications.push(user);

          //If no one else is following, simply display Notification
          if ($scope.notifications.length === 1){
            displayNotification();
          }
        });




        function displayNotification(){
          //Display the notification, when the user wanted it to be displayed
          $timeout(function(){
            $scope.$apply(function(){
              $scope.gotNotification = true;
            });
            hideNotification();
          }, 3000);
        }

        function hideNotification(){
          $timeout(function(){
            $scope.$apply(function(){
              $scope.notifications.splice(0,1);
              $scope.gotNotification = false;
              checkQue();
            })
          }, 5000);
        }

        function checkQue (){
          if ($scope.notifications.length > 0) {
            displayNotification();
          }
        }

        //This is the fire back a transmission
       $scope.test = function(){
         $http.post('http://localhost:3000/techwarriorz/flippy/subscriber').then(function(data){
           console.log(data);
         });
       };

    }])
})(window, window.angular);