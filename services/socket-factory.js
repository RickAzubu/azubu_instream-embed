(function(window, angular, undefined){
  angular.module('embedApp')
    .factory('socket', [function(){
    var socket = io.connect('http://localhost:8080/');
      return{
        on: on,
        emit: emit
      };

      function on(eventName, cb){
        socket.on(eventName, function(user){
          cb(user);
        })
      }

      function emit(eventName, data, cb){
        socket.emit(eventName, data, function(){
          cb();
        })
      }

  }])
})(window, window.angular);