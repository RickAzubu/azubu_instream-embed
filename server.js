var express = require('express'),
    app     = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
http.listen(8080, "127.0.0.1");



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Cache-Control");
  if (req.method === 'OPTIONS') {
    res.statusCode = 204;
    return res.end();
  } else {
    return next();
  }
});

app.use('/libs', express.static(__dirname + '/libs'));
app.use('/styles', express.static(__dirname + '/styles'));
app.use('/controllers', express.static(__dirname + '/controllers'));
app.use('/services', express.static(__dirname + '/services'));
app.use('/config', express.static(__dirname + '/config'));


app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});


//The Web Socket
io.on('connection',function(socket){
  socket.on('connect-broadcaster', function(broadcaster){
    socket.join('overlay_' + broadcaster);
  })
});

app.post('/:broadcaster/:viewer/:type', function(req, res){
    var broadcaster = req.params.broadcaster,
        viewer = {
            username: req.params.viewer,
            type: req.params.type
        };
    var title = "overlay_" + broadcaster;
   io.to(title).emit(title, viewer);
   res.json({status: 200});
});

app.listen('3000', function(){
  console.log("testers");
});